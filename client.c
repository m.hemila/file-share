#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
//#include <sys/types.h>
#include <netinet/in.h>
//#include <sys/socket.h>
#include "myhttp.h"
 
//#define MAXRCVLEN 500
#define PORTNUM 2300

// some of error checking especially network is missing
// Implementation of file sending (text and binary)
// Get files from server (binary)
// at least 1 MByte size
// Robusti poikkeuskäsittely: yhteyden menetys, jos jotain odottamatonta tapahtuu

// Takes a file name to be requested and returns HTTP request for that file
char *make_request(const char *filename) {
    char *request = calloc(5 + strlen(filename) + 11 + 1, 1);
    if(!request) perror("Calloc error for request in make_request");

    strcat(request, "GET /");
    strcat(request, filename);
    strcat(request, " HTTP/1.1\r\n");

    return request;
}

char *file_sending_header(const char *filename, unsigned long len) {
    char *header = calloc(5 + strlen(filename) + 11 + 1, 1);
    if(!header) perror("Calloc error for header in file_sending_header\n");

    strcat(header, "PUT /");
    strcat(header, filename);
    strcat(header, " HTTP/1.1\r\n");
    char middle[100];
    sprintf(middle, "Content-Length: %ld\r\n\r\n", len);
    header = realloc(header, strlen(header) + strlen(middle) + 1);
    strcat(header, middle);

    return header;
}

int main(int argc, char *argv[])
{
    if(argc != 4) {
        printf("Wrong number of arguments! Correct form: \n./client ip-address control-tag filename\n");
        return -1;
    }
    char *ip_char = argv[1];
    int control = *argv[2];
    char *filename = argv[3];
    unsigned long *ip = malloc(sizeof(unsigned long));
    if(inet_pton(AF_INET, ip_char, ip) < 1) {
        printf("IP-address was not in correct form.\n");
	    free(ip);
        return -1;
    }
    if(ip == 0) {
        printf("IP 0.0.0.0 is invalid.\n");
	    free(ip);
        return -1;
    }
    if(control != 'p' && control != 'g') {
        printf("Bad control character. Possible values: p for put, g for get.\n");
	    free(ip);
        return -1;
    }

        

    //char buffer[MAXRCVLEN + 1]; /* +1 so we can add null terminator */
    int mysocket;
    struct sockaddr_in dest; 

    mysocket = socket(AF_INET, SOCK_STREAM, 0);

    memset(&dest, 0, sizeof(dest));                /* zero the struct */
    dest.sin_family = AF_INET;
    //dest.sin_addr.s_addr = htonl(INADDR_LOOPBACK); /* set destination IP number - localhost, 127.0.0.1*/ 
    dest.sin_addr.s_addr = *ip; 
    dest.sin_port = htons(PORTNUM);                /* set destination port number */

    connect(mysocket, (struct sockaddr *)&dest, sizeof(struct sockaddr_in));
    
    if(control == 'g') {
        // HERE FOR TESTING AND ONLY PARTIAL IMPLEMENTATION
        char *tosend = make_request(filename);
        if(!write(mysocket, tosend, strlen(tosend))) perror("Error while writing to socket!\n");
        free(tosend);
        
        struct http_content *response = receive_anything(mysocket);
        if (strncmp(response->text, "HTTP/1.1 200", 12) == 0) {
        	struct http_content *data = extract_content(response->text);
        	printf("File size: %ld\n", data->length);
        	if (store_response(data->text, data->length, filename)) printf("File %s saved.\n", filename);
        	free(data->text);
        	free(data);
        }
        else if (strncmp(response->text, "HTTP/1.1 404", 12) == 0) {
        	printf("File %s not found.\n", filename);
        }
        else printf("Bad HTTP!\n");
        free(response->text);
        free(response);
    }
    else if(control == 'p') {
        char *tosend;
        struct http_content *temp;
        unsigned long len;
        temp = read_file(filename);
        if(!temp) {
            printf("Error while reading file.\n");
	        free(ip);
            return -1;
        }
        char *header = file_sending_header(filename, temp -> length);
        len = temp -> length + strlen(header);
        tosend = calloc(len+1, 1);
        memcpy(tosend, header, strlen(header));
        memcpy(tosend + strlen(header), temp -> text, temp -> length);
        printf("Client sending: %s\n", tosend);
        if(!write(mysocket, tosend, len)) perror("Error while writing to socket\n");
        free(tosend);
        free(header);
        free(temp -> text);
        free(temp);
    }
    else {
        printf("Error in control\n");
	    free(ip);
        return -1;
    }

	free(ip);
    close(mysocket);
    return EXIT_SUCCESS;
}


