struct http_content{
    char *text;
    unsigned long length;
};

void free_http_content(struct http_content *data);
void print_http_content(struct http_content *data);
int send_http(char *content, unsigned long length, int consocket);
struct http_content *read_file(char *filename);
int store_response(const char *response, unsigned long length, const char *filename);
struct http_content *extract_content(char *response);
struct http_content *receive_anything(int socket);
