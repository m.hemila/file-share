#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
//#include <sys/types.h>
#include <netinet/in.h>
//#include <sys/socket.h>
#include "myhttp.h"

#define PORTNUM 2300

// All error checking is missing.
// Listen to multiple clients
// Actual implementation of file receiving and saving
// implementation of sending files back
// at least 1 MByte size and binary
// Robusti poikkeuskäsittely: yhteyden menetys, jos jotain odottamatonta tapahtuu
void error(char *msg)
{
      perror(msg);
      exit(0);
}

void what_to_do(char *request, int consocket) {

	if (strncmp("GET /", request, 5) == 0) {
		int j = 0;
		char filename[strlen(request)];
		memset(filename, 0, strlen(request));
		
		for (int i = 5; i < strlen(request); i++) {
			if (strncmp(" HTTP/1.1", request + i, 9) == 0) break;
			filename[j] = request[i];
			j++;
		}
		//printf("REQUEST: %s\n", request);
		//printf("FILENAME: %s\n", filename);
		struct http_content *content = read_file(filename);
		//printf("CONTENT: %s\n", content->text);
		//printf("LENGTH: %ld\n", content->length);
        if(!content) {
            char tosend[] = "HTTP/1.1 404";
            if(!write(consocket, tosend, strlen(tosend))) error("write Error \n");
        }
        else send_http(content->text, content->length, consocket);
        //if(content) free(content -> text);
        free(content);
	}
    else if (strncmp("PUT /", request, 5) == 0) {
		int j = 0;
		char filename[strlen(request)];
		memset(filename, 0, strlen(request));
		
		for (int i = 5; i < strlen(request); i++) {
			if (strncmp(" HTTP/1.1", request + i, 9) == 0) break;
			filename[j] = request[i];
			j++;
		}

        struct http_content *temp;
        temp = extract_content(request);
        if(temp == NULL) printf("ERROR in extracting the content from response in client\n");
        store_response(temp -> text, temp -> length, filename);
        free(temp -> text);
        free(temp);
    }
    else {
        printf("Bad HTTP!\n");
    }
}

// for handling multiple users
void do_stuff(int consocket) {
		struct http_content *request = receive_anything(consocket);
        what_to_do(request->text, consocket);
        free(request -> text);
        free(request);
        
        close(consocket);
}

int main(int argc, char *argv[])
{
    struct sockaddr_in dest; /* socket info about the machine connecting to us */
    struct sockaddr_in serv; /* socket info about our server */
    int mysocket, pid;            /* socket used to listen for incoming connections */
    socklen_t socksize = sizeof(struct sockaddr_in);

    memset(&serv, 0, sizeof(serv));           /* zero the struct before filling the fields */
    serv.sin_family = AF_INET;                /* set the type of connection to TCP/IP */
    serv.sin_addr.s_addr = htonl(INADDR_ANY); /* set our address to any interface */
    serv.sin_port = htons(PORTNUM);           /* set the server port number */    

    mysocket = socket(AF_INET, SOCK_STREAM, 0);

    /* bind serv information to mysocket */
    bind(mysocket, (struct sockaddr *)&serv, sizeof(struct sockaddr));

    /* start listening, allowing a queue of up to 1 pending connection */
    listen(mysocket, 5);
    int consocket;

    while(1)
    {
        consocket = accept(mysocket, (struct sockaddr *)&dest, &socksize);
        if (consocket < 0) error("ERROR on accept\n");
        pid = fork();
        if (pid < 0) error("ERROR on fork\n");
        if (pid == 0)
        {
            close(mysocket);
            do_stuff(consocket);
            exit(0);
        }
        else close(consocket);
    }

    close(mysocket);
    return EXIT_SUCCESS;
}


