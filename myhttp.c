#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <unistd.h>
//#include <arpa/inet.h>
//#include <sys/types.h>
//#include <netinet/in.h>
#include <sys/socket.h>
#include "myhttp.h"
#include <ctype.h>
#include <sys/stat.h>

off_t fsize(const char *filename) {
    struct stat st; 

    if (stat(filename, &st) == 0)
        return st.st_size;

    return -1; 
}

struct http_content *extract_content(char *response) {
    struct http_content *ret = malloc(sizeof(struct http_content));
    unsigned long length = 0;
    char *content;
    while(1) {
        while(strncmp(response, "\r\n", 2) != 0) {
            response++;
        }
        response += 2;
        if(strncmp(response, "\r\n", 2) == 0) break;
        else if(strncmp(response, "Content-Length:", 15) == 0) {
            response = response + 15;
            while(strncmp(response, "\r\n", 2) != 0) {
                if(isdigit((unsigned char)*response)) length = length * 10 + (*response) - '0';
                response++;
            }
        }
    }
    if(length == 0) {
        printf("Error in finding length in extract_content.\n");
        return NULL;
    }
    response += 2;
    content = malloc(length);
    memcpy(content, response, length);
    ret -> length = length;
    ret -> text = content;
    return ret;
}

void print_http_content(struct http_content *data) {
	for (unsigned long i = 0; i < data->length; i++) {
		fputc(data->text[i], stdout);
	}
}

int send_http(char *content, unsigned long length, int consocket) {
	char *header = calloc(50, 1);
    unsigned int hlen = 0;
    if(!header) perror("Calloc error for header in send_http\n");
    if(length == 0) {
        strcat(header, "HTTP/1.1 404 Not Found");
        send(consocket, header, 50, 0);
        return 1;
    }

	sprintf(header, "HTTP/1.1 200 OK\r\nContent-Length: %ld\r\n\r\n", length);
    hlen = strlen(header);
	
	char *response = calloc(length + hlen, 1);
    if(!response) perror("Calloc error for response in send_http\n");
	strcat(response, header);
	memcpy(response+hlen, content, length);
	
    printf("Sending...");
	send(consocket, response, length+hlen, 0);
	
	free(header);
	free(content);
	free(response);

    return 1;
}

struct http_content *read_file(char *filename) {
	FILE *fp = fopen(filename, "r");
    struct http_content *ret = malloc(sizeof(struct http_content));
    if(!ret) {
        perror("Malloc error for ret in http_content\n");
        return NULL;
    }
	if (!fp) {
		printf("Could not open file %s for reading.\n", filename);
        free(ret);
		return NULL;
	}

    // get length of file (number of bytes to read)
    off_t length = fsize(filename);

    printf("read_file Length: %lu\n", (unsigned long)length);
	char *content = calloc(length, 1);
    if(!content) perror("Malloc error for content in http_content\n");
    if(!fread(content, 1, length, fp)) perror("Error while reading file!\n");
	fclose(fp);

    //content[length] = 0;

    ret -> text = content;
    ret -> length = length;

    return ret;
}

int store_response(const char *response, unsigned long length, const char *filename) {
	FILE *fp;	
	fp = fopen(filename, "w");
	if (!fp) {
		printf("Could not open file %s for writing.\n", filename);
		return -1;
	}
	
    fwrite(response, 1, length, fp);
	
	fclose(fp);
    return 1;
}

struct http_content *receive_anything(int socket) {

	struct http_content *buffer = malloc(sizeof(struct http_content));
	buffer->length = 1024;
	buffer->text = malloc(buffer->length * sizeof(char));
	unsigned long bytes_received = 0;
	
	// vastaanotetaan jonkin verran
	size_t retv = recv(socket, buffer->text, 1024, 0);
	bytes_received += retv;
	
	unsigned long content_length = 0;
	if (retv >= buffer->length) {
		// etsitään content-length
		for (unsigned long i = 0; i < 1024; i++) {
			if (strncmp(buffer->text + i, "Content-Length: ", 16) == 0) {
				char numbers[128];
				memset(numbers, 0, 128);
				for (unsigned long j = 0; j < 127; j++) {
					char c = buffer->text[i + 16 + j];
					if (!isdigit(c)) break;
					numbers[j] = c;
				}
				content_length = atol(numbers);
				break;
			}
		}
		
		// varataan lisää muistia
		buffer->length += content_length;
		buffer->text = realloc(buffer->text, buffer->length);
		
		do {
			retv = 0;
			retv = recv(socket, buffer->text + bytes_received, buffer->length - bytes_received, 0);
			bytes_received += retv;
		} while (retv > 0);
	}
	
	buffer->length = bytes_received;
	printf("Content length: %ld\nBytes received: %ld\n", content_length, bytes_received);
	return buffer;
}
